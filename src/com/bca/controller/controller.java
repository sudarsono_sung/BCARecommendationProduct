package com.bca.controller;

import java.lang.reflect.Type;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import adapter.LogAdapter;
import adapter.RecommendationProductAdapter;

@RestController
public class controller {
    /**
     * 
     */
    final static Logger logger = Logger.getLogger(controller.class);

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String GetPing() {
	String ConnectionStatus = "true";
	return ConnectionStatus;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/id-type/{type}/id-number/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetRecommendationProduct(@PathVariable("type") String loginType, @PathVariable("id") String loginID, @RequestParam(value = "wsid", defaultValue = "") String WSID, @RequestParam(value = "serial", defaultValue = "") String SerialNumber, HttpServletResponse response) {
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;
	model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
	model.mdlAPIResult mdlGetProductResult = new model.mdlAPIResult();

	model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
	model.mdlMessage mdlMessage = new model.mdlMessage();
	Gson gson = new Gson();

	model.mdlLog mdlLog = new model.mdlLog();
	model.mdlRecommendationProduct mdlReccomendationProduct = new model.mdlRecommendationProduct();

	mdlLog.WSID = WSID;
	mdlLog.SerialNumber = SerialNumber;
	mdlLog.ApiFunction = "RecommendationProduct";
	mdlLog.SystemFunction = "GetRecommendationProduct";
	mdlLog.LogSource = "Webservice";
	mdlLog.LogStatus = "Failed";

	String APIName = "BCARecommendationProduct";
	String jsonIn = "type:" + loginType + ", id:" + loginID + ", WSID:" + WSID + ", SerialNumber:" + SerialNumber;
	String jsonOut = "";
	String cinCustomer = "";

	try {
	    if (loginType == null || loginType.equals("") || loginID == null || loginID.equals("") || WSID == null || WSID.equals("") || SerialNumber == null || SerialNumber.equals("")) {
		mdlErrorSchema.ErrorCode = "01";
		mdlMessage.Indonesian = "type / id / wsid / serial tidak valid";
		mdlMessage.English = mdlLog.ErrorMessage = "type / id / wsid / serial not valid";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetProductResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		jsonOut = gson.toJson(mdlGetProductResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		logger.error("FAILED. API : " + APIName + ", method : GET, Time : " + elapsedTime + " ms, parameterIn:" + jsonIn + ", jsonOut:" + jsonOut);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

		return jsonOut;
	    }

	    if (loginType.equalsIgnoreCase("atm")) {
		cinCustomer = loginID;
	    } else if (loginType.equalsIgnoreCase("ktp")) {

		mdlAPIResult = RecommendationProductAdapter.getCISByKTP(loginID, WSID, SerialNumber);
		if (mdlAPIResult == null || mdlAPIResult.ErrorSchema.ErrorCode == null) {
		    mdlErrorSchema.ErrorCode = "02";
		    mdlMessage.Indonesian = "Gagal memanggil service";
		    mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
		    mdlErrorSchema.ErrorMessage = mdlMessage;
		    mdlGetProductResult.ErrorSchema = mdlErrorSchema;
		    LogAdapter.InsertLog(mdlLog);
		    jsonOut = gson.toJson(mdlGetProductResult);
		    stopTime = System.currentTimeMillis();
		    elapsedTime = stopTime - startTime;
		    logger.error("FAILED. API : " + APIName + ", method : GET, Time : " + elapsedTime + " ms, parameterIn:" + jsonIn + ", jsonOut:" + jsonOut);
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    return jsonOut;
		} else if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		    if (mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-99-731")) {
			// Error ESB-99-731 - Nomor Customer tidak ditemukan,
			// cin jadi default 00000000000
			cinCustomer = "00000000000";
		    } else {
			mdlErrorSchema.ErrorCode = mdlAPIResult.ErrorSchema.ErrorCode;
			mdlMessage.Indonesian = mdlAPIResult.ErrorSchema.ErrorMessage.Indonesian;
			mdlMessage.English = mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
			mdlErrorSchema.ErrorMessage = mdlMessage;
			mdlGetProductResult.ErrorSchema = mdlErrorSchema;
			LogAdapter.InsertLog(mdlLog);
			jsonOut = gson.toJson(mdlGetProductResult);
			stopTime = System.currentTimeMillis();
			elapsedTime = stopTime - startTime;
			logger.error("FAILED. API : " + APIName + ", method : GET, Time : " + elapsedTime + " ms, parameterIn:" + jsonIn + ", jsonOut:" + jsonOut);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return jsonOut;
		    }

		} else {
		    model.mdlCISByKTP mdlCISByKTP = new model.mdlCISByKTP();
		    String OutputSchemaString = gson.toJson(mdlAPIResult.OutputSchema);
		    mdlCISByKTP = gson.fromJson(OutputSchemaString, model.mdlCISByKTP.class);
		    List<model.mdlCIS> listCIS = mdlCISByKTP.CIS;
		    if (listCIS.size() == 0) {
			cinCustomer = "00000000000";
		    } else if (listCIS.size() == 1) {
			String firstCISNumber = listCIS.get(0).CISCustomerNumber;
			cinCustomer = firstCISNumber.equalsIgnoreCase("") || firstCISNumber == null ? "00000000000" : firstCISNumber;
		    } else {
			for (model.mdlCIS cis : listCIS) {
			    String CISNumber = cis.CISCustomerNumber;
			    cinCustomer = CISNumber.equalsIgnoreCase("") || CISNumber == null ? "00000000000" : CISNumber;
			}
			// for (model.mdlAccount account : listAccount) { //
			// looping
			// // account
			// // list
			// Integer customerCounter =
			// account.CustomerDetails.size();
			// if (customerCounter == 1) {
			// // if only have 1 customer details, then save
			// // first single account index to variable
			// cinCustomer =
			// account.CustomerDetails.get(0).CustomerNumber;
			// break;
			// } else {
			// // if have multiple customer details, check for
			// List<model.mdlCustomer> listCustomer =
			// account.CustomerDetails;
			// Integer customerIndex = 0;
			// Integer accountPrimaryCustomerIndex = 0;
			// for (model.mdlCustomer customer : listCustomer) {
			// if (customer.OwnerCode.equalsIgnoreCase("901")) {
			// accountPrimaryCustomerIndex = customerIndex;
			// }
			// customerIndex++;
			// }
			// cinCustomer =
			// account.CustomerDetails.get(accountPrimaryCustomerIndex).CustomerNumber;
			// }
			// }
		    }
		}
	    }

	    // after get CIN, call recommendation product API
	    if (cinCustomer.equals("")) {
		mdlErrorSchema.ErrorCode = "03";
		mdlMessage.Indonesian = "Customer Identification Number tidak valid";
		mdlMessage.English = mdlLog.ErrorMessage = "Customer Identification Number not valid";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetProductResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		jsonOut = gson.toJson(mdlGetProductResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		logger.error("FAILED. API : " + APIName + ", method : GET, Time : " + elapsedTime + " ms, parameterIn:" + jsonIn + ", jsonOut:" + jsonOut);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return jsonOut;
	    }

	    mdlAPIResult = new model.mdlAPIResult();
	    mdlAPIResult = RecommendationProductAdapter.getRecommendationProduct(cinCustomer, WSID, SerialNumber);
	    if (mdlAPIResult == null || mdlAPIResult.ErrorSchema.ErrorCode == null) {
		mdlErrorSchema.ErrorCode = "04";
		mdlMessage.Indonesian = "Gagal memanggil service";
		mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetProductResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		jsonOut = gson.toJson(mdlGetProductResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		logger.error("FAILED. API : " + APIName + ", method : GET, Time : " + elapsedTime + " ms, parameterIn:" + jsonIn + ", jsonOut:" + jsonOut);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return jsonOut;
	    } else if (mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-15-092")) {
		jsonOut = "{\"error-schema\": {\"error-code\": \"00\",\"error-message\": {\"indonesian\": \"Berhasil\",\"english\": \"Success\"}},\"output-schema\": {\"cin\": \"" + cinCustomer + "\",\"recommendation-product\": null,\"category-product\": null}}";
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		logger.info("SUCCESS. API : " + APIName + ", method : POST, Time : " + elapsedTime + " ms, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
		LogAdapter.InsertLog(mdlLog);
		return jsonOut;
	    } else if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlErrorSchema.ErrorCode = mdlAPIResult.ErrorSchema.ErrorCode;
		mdlMessage.Indonesian = mdlAPIResult.ErrorSchema.ErrorMessage.Indonesian;
		mdlMessage.English = mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetProductResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		jsonOut = gson.toJson(mdlGetProductResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		logger.error("FAILED. API : " + APIName + ", method : GET, Time : " + elapsedTime + " ms, parameterIn:" + jsonIn + ", jsonOut:" + jsonOut);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return jsonOut;
	    }

	    LinkedTreeMap<String, JsonArray> outputSchemaMap = (LinkedTreeMap<String, JsonArray>) mdlAPIResult.OutputSchema;
	    JsonObject mObj = gson.toJsonTree(outputSchemaMap).getAsJsonObject();
	    Type collectionType = new TypeToken<List<model.mdlProduct>>() {
	    }.getType();

	    // insert company name based on company name list in JSON
	    List<model.mdlProduct> listProduct = gson.fromJson(mObj.get("recommendation-product").toString(), collectionType);
	    collectionType = new TypeToken<List<model.mdlCategoryProduct>>() {
	    }.getType();
	    List<model.mdlCategoryProduct> listCategory = gson.fromJson(mObj.get("category-product").toString(), collectionType);
	    Integer productIndex = 0;
	    for (model.mdlProduct product : listProduct) {
		product.CompanyName = "";
		for (model.mdlCategoryProduct category : listCategory) {
		    if (category.CompanyCode.equalsIgnoreCase(product.CompanyCode)) {
			product.CompanyName = category.CompanyName;
		    }
		}
		listProduct.set(productIndex, product);
		productIndex++;
	    }

	    mdlReccomendationProduct.CIN = cinCustomer;
	    mdlReccomendationProduct.RecommendationProduct = listProduct;
	    mdlReccomendationProduct.CategoryProduct = listCategory;
	    String outputResult = gson.toJson(mdlReccomendationProduct);
	    Object result = gson.fromJson(outputResult, Object.class);

	    mdlErrorSchema.ErrorCode = "00";
	    mdlMessage.Indonesian = "Berhasil";
	    mdlMessage.English = mdlLog.LogStatus = "Success";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlGetProductResult.ErrorSchema = mdlErrorSchema;
	    mdlGetProductResult.OutputSchema = result;
	    jsonOut = gson.toJson(mdlGetProductResult);
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.info("SUCCESS. API : " + APIName + ", method : POST, Time : " + elapsedTime + " ms, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);

	} catch (Exception ex) {
	    mdlErrorSchema.ErrorCode = "05";
	    mdlMessage.Indonesian = "Gagal memanggil service";
	    mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlGetProductResult.ErrorSchema = mdlErrorSchema;
	    mdlLog.ErrorMessage = ex.toString();
	    jsonOut = gson.toJson(mdlGetProductResult);
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.error("FAILED. API : " + APIName + ", method : GET, Time : " + elapsedTime + " ms, parameterIn:" + jsonIn + ", jsonOut:" + jsonOut + ", Exception : " + ex.toString(), ex);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	}

	LogAdapter.InsertLog(mdlLog);
	return jsonOut;
    }

}
