package adapter;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class RecommendationProductAdapter {
    final static Logger logger = Logger.getLogger(RecommendationProductAdapter.class);
    
    static Client client = Client.create();

    public static model.mdlAPIResult getCISByKTP(String ktpNumber, String WSID, String SerialNumber) {
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;
	model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "RecommendationProduct";
	mdlLog.SystemFunction = "getAccountListByKTP";
	mdlLog.LogSource = "Middleware";
	mdlLog.SerialNumber = SerialNumber;
	mdlLog.WSID = WSID;
	mdlLog.LogStatus = "Success";
	mdlLog.ErrorMessage = "";

	String jsonIn = "type:ktp, id:" + ktpNumber + ", WSID:" + WSID + ", SerialNumber:" + SerialNumber;
	String jsonOut = "";
	Gson gson = new Gson();

	String urlAPI = "/cis/search/db2?SearchBy=IdNumber&IdNumber=" + ktpNumber;
	try {
	    // Get the base naming context from web.xml
	    Context context = (Context) new InitialContext().lookup("java:comp/env");

	    // Get a single value from web.xml
	    String MiddlewareIpAddress = (String) context.lookup("param_ipmiddleware");
	    String urlFinal = MiddlewareIpAddress + urlAPI;
	    String keyAPI = (String) context.lookup("param_keyAPI");
	    String ClientID = (String) context.lookup("param_clientid");
	    String decryptedClientID = EncryptAdapter.decrypt(ClientID, keyAPI);
	    
//	    Client client = Client.create();
	    WebResource webResource = client.resource(urlFinal);
	    ClientResponse response = webResource.type("application/json").header("ClientID", decryptedClientID).get(ClientResponse.class);
	    jsonOut = response.getEntity(String.class);
	    
	    stopTime = System.currentTimeMillis();
	    

	    mdlAPIResult = gson.fromJson(jsonOut, model.mdlAPIResult.class);
	    if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
	    }
	    elapsedTime = stopTime - startTime;
	    logger.info("SUCCESS. API : BCARecommendationProduct, method : GET, Time :" + elapsedTime + " ms, function : getCISByKTP, jsonIn:" 
		    		+ jsonIn + ", jsonOut:" + jsonOut);
	} catch (Exception ex) {
	    mdlAPIResult = null;
	    mdlLog.LogStatus = "Failed";
	    logger.error(
		    "FAILED. API : BCARecommendationProduct, method : GET, Time:" + elapsedTime + " ms, function : getCISByKTP, jsonIn:" 
			    	+ jsonIn + ", Exception : "+ ex.toString(),ex);
	    mdlLog.ErrorMessage = ex.toString();
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlAPIResult;
    }
    
    public static model.mdlAPIResult getRecommendationProduct(String cinCustomer, String WSID, String SerialNumber) {
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;
	model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "RecommendationProduct";
	mdlLog.SystemFunction = "getRecommendationProduct";
	mdlLog.LogSource = "Middleware";
	mdlLog.SerialNumber = SerialNumber;
	mdlLog.WSID = WSID;
	mdlLog.LogStatus = "Success";
	mdlLog.ErrorMessage = "";
	String jsonIn = "CIN:" + cinCustomer + ", WSID:" + WSID + ", SerialNumber:" + SerialNumber;
	String jsonOut = "";
	Gson gson = new Gson();
	
	String urlAPI = "/Assurance/v2/referral/" + cinCustomer + "/recommendation-product";
	
	try {
	    // Get the base naming context from web.xml
	    Context context = (Context) new InitialContext().lookup("java:comp/env");
	    // Get a single value from web.xml
	    String MiddlewareIpAddress = (String) context.lookup("param_ipmiddleware");
	    String urlFinal = MiddlewareIpAddress + urlAPI;
	    String keyAPI = (String) context.lookup("param_keyAPI");
	    String ClientID = (String) context.lookup("param_clientid");
	    String decryptedClientID = EncryptAdapter.decrypt(ClientID, keyAPI);
	    
//	    Client client = Client.create();
	    WebResource webResource = client.resource(urlFinal);
	    ClientResponse response = webResource.type("application/json").header("client-id", decryptedClientID).get(ClientResponse.class);
	    jsonOut = response.getEntity(String.class);

	    mdlAPIResult = gson.fromJson(jsonOut, model.mdlAPIResult.class);
	    if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
	    }
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.info(
		    "SUCCESS. API : BCARecommendationProduct, method : GET, function : getRecommendationProduct, jsonIn:" + jsonIn 
		    		+ ", Time :" + elapsedTime + " ms, jsonOut:" + jsonOut);
	} catch (Exception ex) {
	    mdlAPIResult = null;
	    mdlLog.LogStatus = "Failed";
	    logger.error(
		    "FAILED. API : BCARecommendationProduct, method : GET, function : getRecommendationProduct, jsonIn:" + jsonIn 
		    		+ ", jsonOut: " + jsonOut + ",Exception : "
		    		+ ex.toString(),
		    ex);
	    mdlLog.ErrorMessage = ex.toString();
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlAPIResult;
    }
}
