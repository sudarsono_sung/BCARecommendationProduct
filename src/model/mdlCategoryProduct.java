package model;

import com.google.gson.annotations.SerializedName;

public class mdlCategoryProduct {
    @SerializedName(value="company-code", alternate="CompanyCode")
    public String CompanyCode;
    
    @SerializedName(value="company-name", alternate="CompanyName")
    public String CompanyName;
}
