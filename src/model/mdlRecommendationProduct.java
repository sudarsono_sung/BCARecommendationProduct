package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlRecommendationProduct {
    
    @SerializedName(value="cin", alternate="CIN")
    public String CIN;
    
    @SerializedName(value="recommendation-product", alternate="RecommendationProduct")
    public List<model.mdlProduct> RecommendationProduct;
    
    @SerializedName(value="category-product", alternate="CategoryProduct")
    public List<model.mdlCategoryProduct> CategoryProduct;
}
