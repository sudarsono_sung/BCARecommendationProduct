package model;

import com.google.gson.annotations.SerializedName;

public class mdlErrorSchema {
    @SerializedName(value="error-code", alternate="ErrorCode")
    public String ErrorCode;
    @SerializedName(value="error-message", alternate="ErrorMessage")
    public model.mdlMessage ErrorMessage;
}
