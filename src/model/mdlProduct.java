package model;

import com.google.gson.annotations.SerializedName;

public class mdlProduct {
    @SerializedName(value="company-code", alternate="CompanyCode")
    public String CompanyCode;
    
    @SerializedName(value="company-name", alternate="CompanyName")
    public String CompanyName;
    
    @SerializedName(value="product-code", alternate="ProductCode")
    public String ProductCode;
    
    @SerializedName(value="product-name", alternate="ProductName")
    public String ProductName;
    
    @SerializedName(value="product-priority", alternate="ProductPriority")
    public String ProductPriority;
    
    @SerializedName(value="product-link", alternate="ProductLink")
    public model.mdlProductLink ProductLink;
    
    @SerializedName(value="last-product", alternate="LastProduct")
    public model.mdlLastProduct LastProduct;
}