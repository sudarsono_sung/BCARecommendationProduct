package model;

import com.google.gson.annotations.SerializedName;

public class mdlAPIResult {
    @SerializedName(value = "error-schema", alternate = "ErrorSchema")
    public model.mdlErrorSchema ErrorSchema;
    @SerializedName(value = "output-schema", alternate = "OutputSchema")
    public Object OutputSchema;
}