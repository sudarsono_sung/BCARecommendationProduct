package model;

import com.google.gson.annotations.SerializedName;

public class mdlProductLink{

    @SerializedName(value="url-mybca", alternate="UrlMybca")
    String UrlMybca;

    @SerializedName(value="url-bcacoid", alternate="UrlBcacoid")
    String UrlBcacoid;
    
    @SerializedName(value="url-futurebranch", alternate="UrlFuturebranch")
    String UrlFuturebranch;
    
    @SerializedName(value="url-futurebranch-icon", alternate="UrlFuturebranchIcon")
    String UrlFuturebranchIcon;
    
    @SerializedName(value="url-futurebranch-ads", alternate="UrlFuturebranchAds")
    String UrlFuturebranchAds;
}
