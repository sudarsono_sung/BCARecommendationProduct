package model;

import com.google.gson.annotations.SerializedName;

public class mdlLastProduct {
    @SerializedName(value = "product-code", alternate = "ProductCode")
    public String ProductCode;

    @SerializedName(value = "referral-date", alternate = "ReferralDate")
    public String ReferralDate;
}
